﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gitFractions
{
    class Fraction
    {
        private int point, num, denum;

        public int Point
        {
            set { this.point = value; }
            get { return this.point; }
        }

        public int Num
        {
            set { this.num = value; }
            get { return this.num; }
        }

        public int Denum
        {
            set { this.denum = value; }
            get { return this.denum; }
        }

        public Fraction()
        {
            this.point = 0;
            this.num = 0;
            this.denum = 0;
        }

        public Fraction(int point, int num, int denum)
        {
            this.point = point;
            this.num = num;
            this.denum = denum;
        }

        public Fraction(int num, int denum)
        {
            this.point = 0;
            this.num = num;
            this.denum = denum;
        }

        public static explicit operator double(Fraction fr1)
        {
            return (double)fr1.Num / (double)fr1.Denum + (double)fr1.Point;
        }

        public static explicit operator Fraction(double fr1)
        {
            Fraction tmp = new Fraction();
            if (fr1 > 0.0)
            {
                string tmpStr = fr1.ToString();
                int counter = 0;
                bool flag = true;

                if (tmpStr.IndexOf(',') > 0)
                    counter = tmpStr.Remove(0, tmpStr.IndexOf(',')).Length - 1;

                if ((int)fr1 != 0)
                {
                    tmp.Point = (int)fr1;
                    fr1 -= tmp.Point;
                }
                else
                    tmp.Point = 0;

                for (int i = 0; i < counter; i++)
                    fr1 *= 10;

                tmp.Num = (int)fr1;
                tmp.Denum = (int)Math.Pow(10, counter);

                for (int i = 2; flag && i < 10; i++)
                {
                    while (tmp.Num % i == 0 && tmp.Denum % i == 0)
                    {
                        tmp.Num /= i;
                        tmp.Denum /= i;
                        flag = false;
                    }
                }
            }
            return tmp;
        }

        public static explicit operator Fraction(int fr1)
        {
            return new Fraction(fr1, 0, 0);
        }

        public static explicit operator int(Fraction fr1)
        {
            return fr1.Point;
        }

        static public bool operator >(Fraction fr1, Fraction fr2)
        {
            return (double)fr1 > (double)fr2;
        }

        static public bool operator <(Fraction fr1, Fraction fr2)
        {
            return (double)fr1 < (double)fr2;
        }

        static public bool operator ==(Fraction fr1, Fraction fr2)
        {
            return (double)fr1 == (double)fr2;
        }

        static public bool operator !=(Fraction fr1, Fraction fr2)
        {
            return (double)fr1 != (double)fr2;
        }

        static public Fraction operator +(Fraction fr1, Fraction fr2)
        {
            double tmp = (double)fr1 + (double)fr2;
            return (Fraction)tmp;
        }

        static public Fraction operator +(int fr1, Fraction fr2)
        {
            return new Fraction(fr2.Point + fr1, fr2.Num, fr2.Denum);
        }

        static public Fraction operator +(Fraction fr1, int fr2)
        {
            return new Fraction(fr1.Point + fr2, fr1.Num, fr1.Denum);
        }

        static public Fraction operator +(double fr1, Fraction fr2)
        {
            return (Fraction)fr1 + fr2;
        }

        static public Fraction operator +(Fraction fr1, double fr2)
        {
            return (Fraction)fr2 + fr1;
        }

        static public Fraction operator *(Fraction fr1, int fr2)
        {
            return (Fraction)((double)fr1 * fr2);
        }

        static public Fraction operator *(int fr1, Fraction fr2)
        {
            return (Fraction)((double)fr2 * fr1);
        }

        public override string ToString()
        {
            if (this.Point <= 0)
            {
                if (this.Num == 0 && this.Denum == 0)
                    return String.Format("0");
                else
                    return String.Format("{0}/{1}", this.Num, this.Denum);
            }
            else
            {
                if (this.Num == 0 && this.Denum == 0)
                    return String.Format("{0}", this.Point);
                else
                    return String.Format("{0} {1}/{2}", this.Point, this.Num, this.Denum);
            }
        }
    }
}
